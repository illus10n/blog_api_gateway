package helpers

import (
	"errors"
	"net"
	"regexp"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
)

func ValidatePassword(password string) error {
	if password == "" {
		return errors.New("password cannot be blank")
	}
	if len(password) < 4 || len(password) > 30 {
		return errors.New("password length should be 4 to 30 characters")
	}
	if validation.Validate(password, validation.Match(regexp.MustCompile("^[A-Za-z0-9$_@.#]+$"))) != nil {
		return errors.New("password should contain only alphabetic characters, numbers and special characters(@, $, _, ., #)")
	}
	return nil
}

func ValidateLogin(login string) error {
	if login == "" {
		return errors.New("login cannot be blank")
	}
	if len(login) < 6 && len(login) > 15 {
		return errors.New("login length should be 8 to 30 characters")
	}
	if validation.Validate(login, validation.Match(regexp.MustCompile("^[A-Za-z0-9$@_.#]+$"))) != nil {
		return errors.New("login should contain only alphabetic characters, numbers and special characters(@, $, _, ., #)")
	}
	return nil
}

// ValidateEmail checks if the email provided passes the required structure
// and length test. It also checks the domain has a valid MX record.
func ValidateEmail(e string) error {
	if len(e) < 5 && len(e) > 128 {
		return errors.New("email length should be 5 to 128 characters")
	}

	if validation.Validate(e, validation.Match(regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"))) != nil {
		return errors.New("Invalid email, make sure you have correct email format")
	}

	parts := strings.Split(e, "@")
	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) == 0 {
		return errors.New("Invalid email, make sure you entered correct email address")
	}
	return nil
}
