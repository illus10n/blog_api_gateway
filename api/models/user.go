package models

//CreateUserModel ...
type CreateUserModel struct {
	Username  string `json:"username"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Password  string `json:"password"`
	Email     string `json:"email"`
}

//UpdateUserProfileModel ...
type UpdateUserProfileModel struct {
	Username  string `json:"username"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Password  string `json:"password"`
	Email     string `json:"email"`
	Address   string `json:"address"`
}

//GetUserModel ...
type GetUserModel struct {
	ID          string `json:"id"`
	AccessToken string `json:"access_token"`
	Username    string `json:"username"`
	Firstname   string `json:"firstname"`
	Lastname    string `json:"lastname"`
	Address     string `json:"address"`
	Email       string `json:"email"`
	IsActive    bool   `json:"is_active"`
	IsAdmin     bool   `json:"is_admin"`
	CreatedAt   string `json:"created_at"`
}

//GetAllUsersModel ...
type GetAllUsersModel struct {
	Count int            `json:"count,string"`
	Users []GetUserModel `json:"Users"`
}

//UserLoginRequest ...
type UserLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

//EmailVerification ...
type EmailVerification struct {
	Code  string `json:"code"`
	Email string `json:"email"`
}

//ConfirmEmailRequest ...
type ConfirmEmailRequest struct {
	Code string `json:"code"`
}

//ConfirmUserLoginRequest ...
type ConfirmUserLoginRequest struct {
	Code  string `json:"code"`
	Email string `json:"Email"`
}

//ConfirmUserLoginResponse ...
type ConfirmUserLoginResponse struct {
	ID          string `json:"id"`
	AccessToken string `json:"access_token"`
}

//SearchByEmailResponse ...
type SearchByEmailResponse struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Email    string `json:"Email"`
}

//FindUsersRequestModel ...
type FindUsersRequestModel struct {
	Page     int64  `json:"page,string"`
	Search   string `json:"search"`
	Active   bool   `json:"active"`
	Inactive bool   `josn:"inactive"`
	Limit    int64  `json:"limit,string"`
	Sort     string `json:"sort" example:"name|asc"`
}
