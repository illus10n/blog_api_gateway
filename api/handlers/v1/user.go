package v1

import (
	"context"
	"encoding/json"
	"errors"
	pbu "genproto/user_service"
	"net/http"

	"bitbucket.org/illus10n/blog_api_gateway/api/helpers"
	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"github.com/google/uuid"

	"bitbucket.org/illus10n/blog_api_gateway/api/models"
	"bitbucket.org/illus10n/blog_api_gateway/pkg/etc"
	"bitbucket.org/illus10n/blog_api_gateway/pkg/jwt"
	"bitbucket.org/illus10n/blog_api_gateway/pkg/logger"
)

// @Router /v1/user/register [post]
// @Summary User Registration
// @Description API for user registration
// @Tags user
// @Accept  json
// @Produce  json
// @Param user body models.CreateUserModel true "user"
// @Success 200 {object} models.GetUserModel
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) RegisterUser(c *gin.Context) {
	ErrAlreadyExists = errors.New("Already exists")
	var (
		jspbMarshal   jsonpb.Marshaler
		jspbUnmarshal jsonpb.Unmarshaler
		user          pbu.User
		code          string
	)

	jspbMarshal.OrigName = true

	err := jspbUnmarshal.Unmarshal(c.Request.Body, &user)
	if handleInternalWithMessage(c, h.log, err, "Error while unmarshalling") {
		return
	}

	// validate email
	err = helpers.ValidateEmail(user.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: err.Error(),
		})
		return
	}

	// validate password
	err = helpers.ValidatePassword(user.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: err.Error(),
		})
		return
	}

	// checking for existance by email
	existingUser, err := h.grpcClient.UserService().Get(
		context.Background(),
		&pbu.GetUserRequest{
			Email: user.Email,
		},
	)
	if existingUser != nil {
		// returning error message if the given email is already used
		if existingUser.User.EmailVerified && handleStorageErrWithMessage(c, h.log, ErrAlreadyExists, "Error while creating user") {
			return
		}

		// removing user with this email unverified
		_, err = h.grpcClient.UserService().Delete(
			context.Background(),
			&pbu.DeleteRequest{
				Id: existingUser.User.Id,
			},
		)
		if handleGrpcErrWithMessage(c, h.log, err, "Error while removing user entity") {
			return
		}
	}
	// todo: send verification email

	// // sending sms code
	// code = etc.GenerateCode(6)
	// _, err = h.grpcClient.SmsService().Send(
	// 	context.Background(), &pbs.Sms{
	// 		Text:       code,
	// 		Recipients: []string{user.Email},
	// 	},
	// )
	// if handleGrpcErrWithMessage(c, h.log, err, "Error while sending sms") {
	// 	return
	// }
	// _, err = h.grpcClient.UserService().SendVerificationCode(
	// 	context.Background(),
	// 	&pbu.EmailVerification{
	// 		Email: user.Email,
	// 		Code:  code,
	// 	},
	// )
	// if handleGrpcErrWithMessage(c, h.log, err, "Error while creating email verification entity") {
	// 	return
	// }

	// generating hashed password
	passwordHash, err := etc.GeneratePasswordHash(user.Password)
	if handleInternalWithMessage(c, h.log, err, "Error while generating password hash") {
		return
	}

	// generating id for new user
	id, err := uuid.NewRandom()
	if handleInternalWithMessage(c, h.log, err, "Error while generating UUID") {
		return
	}

	// generating access token for new user
	accessToken, err := jwt.GenerateJWT(id.String(), "user", signingKey)
	if handleInternalWithMessage(c, h.log, err, "Error while generating access token") {
		return
	}

	user.Id = id.String()
	user.AccessToken = accessToken
	user.Password = string(passwordHash)

	res, err := h.grpcClient.UserService().Create(
		context.Background(),
		&user,
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while creating user") {
		return
	}

	js, err := jspbMarshal.MarshalToString(res.User)
	if handleInternalWithMessage(c, h.log, err, "Error while marshalling") {
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// @Router /v1/user/verify-email [post]
// @Summary User Email Verification
// @Description API for checking whether email is valid, `Authorization` header is required
// @Tags user
// @Accept  json
// @Produce  json
// @Param verification body models.EmailVerification true "user"
// @Success 200 {object} models.ConfirmEmailRequest
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) VerifyUserEmail(c *gin.Context) {
	var (
		model models.ConfirmEmailRequest
	)

	err := c.ShouldBindJSON(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	// getting user info from Authorization header
	user, err := userInfo(h, c)
	if err != nil {
		return
	}

	// getting user using user ID
	user, err := h.grpcClient.UserService().Get(
		context.Background(),
		&pbu.GetUserRequest{
			Id: user.ID,
		})
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	// verifying email
	res, err := h.grpcClient.UserService().VerifyEmail(
		context.Background(),
		&pbu.EmailVerification{
			Email: user.User.Email,
			Code:  model.Code,
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	c.JSON(http.StatusOK, res)
}

// @Router /v1/user/login [post]
// @Summary User Login
// @Description API for user login
// @Tags user
// @Accept  json
// @Produce  json
// @Param user body models.UserLoginRequest true "user"
// @Success 200 {object} models.GetUserModel
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) LoginUser(c *gin.Context) {
	var (
		model   models.UserLoginRequest
		isMatch = true
	)
	err := c.ShouldBindJSON(&model)

	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	user, err := h.grpcClient.UserService().GetByLogin(
		context.Background(),
		&pbu.User{
			Email: model.Email,
		})
	if err != nil {
		isMatch = false
	} else {
		h.log.Debug("Comparing hash and password", logger.String(model.Password, "s"))
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(model.Password))
		if err != nil {
			isMatch = false
		}
	}

	if !isMatch {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: "login or password is incorrect",
			},
		})
		return
	}

	c.JSON(http.StatusOK, user)
}

// @Router /v1/user/profile [get]
// @Summary Get Active User with `Authorization` Header
// @Description API for getting active (logged in) user with `Authorization` Header
// @Tags user
// @Accept  json
// @Produce  json
// @Success 200 {object} models.GetUserModel
// @Failure 401 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) GetActiveUser(c *gin.Context) {
	var (
		model models.UserInfo
	)

	model, err := userInfo(h, c)

	if err != nil {
		return
	}

	user, err := h.grpcClient.UserService().Get(
		context.Background(),
		&pbu.GetUserRequest{
			Id: model.ID,
		})
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	c.JSON(http.StatusOK, user)
}

// @Router /v1/user [get]
// @Summary List Users
// @Description API for getting users according to filters
// @Tags user
// @Accept  json
// @Produce  json
// @Param filters query models.ListUsersRequestModel false "filters"
// @Success 200 {object} models.ListProductsModel
// @Failure 400 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) ListUsers(c *gin.Context) {
	var (
		marshaller jsonpb.Marshaler
		model      models.GetAllUsersModel
	)
	marshaller.OrigName = true
	page, err := ParsePageQueryParam(c)
	limit, err := ParseLimitQueryParam(c)
	search, err := ParseSearchQueryParam(c)
	active, err := ParseActiveQueryParam(c)
	inactive, err := ParseInactiveQueryParam(c)

	if handleGrpcErrWithMessage(c, h.log, err, "error while parsing filter query") {
		return
	}

	resp, err := h.grpcClient.UserService().List(
		context.Background(),
		&pbu.ListRequest{
			Page:     int64(page),
			Limit:    int64(limit),
			Search:   string(search),
			Active:   bool(active),
			Inactive: bool(inactive),
		},
	)

	if handleGrpcErrWithMessage(c, h.log, err, "error while finding users") {
		return
	}

	js, _ := marshaller.MarshalToString(resp)

	err = json.Unmarshal([]byte(js), &model)

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorCodeInternal,
				Message: "error while parsing proto to struct",
			},
		})
		h.log.Error("error while parsing proto to struct", logger.Error(err))
		h.log.Error("error of response" + js)
		return
	}

	c.JSON(http.StatusOK, model)
}

// @Router /v1/user/profile [put]
// @Summary Update User Profile
// @Description API for updating user profile. `Authorization` required
// @Tags user
// @Accept  json
// @Produce  json
// @Param user body models.UpdateUserProfileModel true "user"
// @Success 200 {object} models.GetUserModel
// @Failure 401 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *handlerV1) UpdateUserProfile(c *gin.Context) {
	var (
		unmarshal  jsonpb.Unmarshaler
		updateUser pbu.User
	)

	// checking authorization
	user, err := userInfo(h, c)
	if err != nil {
		return
	}

	// getting user using user ID (checking if user with this id exists)
	user, err := h.grpcClient.UserService().Get(
		context.Background(),
		&pbu.GetUserRequest{
			Id: user.ID,
		})
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	err = unmarshal.Unmarshal(c.Request.Body, &updateUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Code:    ErrorBadRequest,
				Message: "error while parsing json to proto",
			},
		})
		h.log.Error("error while parsing json to proto", logger.Error(err))
		return
	}

	// if password is given, we are going to update password
	if updateUser.GetPassword() != "" {
		err = helpers.ValidatePassword(updateUser.Password)
		if err != nil {
			c.JSON(http.StatusBadRequest, models.ResponseError{
				Error: err.Error(),
			})
			return
		}

		passwordHash, err := etc.GeneratePasswordHash(updateUser.Password)
		if handleInternalWithMessage(c, h.log, err, "Error while generating password hash") {
			return
		}
		updateUser.Password = string(passwordHash)
	}

	updateUser.Id = user.User.Id

	resp, err := h.grpcClient.UserService().Update(
		context.Background(),
		&updateUser,
	)
	if handleGrpcErrWithMessage(c, h.log, err, "error while updating user") {
		return
	}

	c.JSON(http.StatusOK, resp)
}
