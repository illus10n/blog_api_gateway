package api

import (
	"net/http"

	//"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	// _ "bitbucket.org/illus10n/blog_api_gateway/api/docs" //for swagger
	v1 "bitbucket.org/illus10n/blog_api_gateway/api/handlers/v1"
	"bitbucket.org/illus10n/blog_api_gateway/config"
	"bitbucket.org/illus10n/blog_api_gateway/pkg/grpc_client"

	//"bitbucket.org/illus10n/blog_api_gateway/pkg/http/middleware"
	"bitbucket.org/illus10n/blog_api_gateway/pkg/logger"
)

//Config ...
type Config struct {
	Logger     logger.Logger
	GrpcClient *grpc_client.GrpcClient
	Cfg        config.Config
}

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(cnf Config) *gin.Engine {
	r := gin.New()

	r.Static("/images", "./static/images")

	r.Use(gin.Logger())

	r.Use(gin.Recovery())

	//r.Use(middleware.NewAuthorizer(cnf.CasbinEnforcer))

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = append(config.AllowHeaders, "Authorization")
	config.AllowHeaders = append(config.AllowHeaders, "image/jpeg")
	config.AllowHeaders = append(config.AllowHeaders, "image/png")

	r.Use(cors.New(config))

	//r.Use(func(context *gin.Context) {
	//	context.Header("Access-Control-Allow-Origin", "*")
	//	context.Header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH")
	//	context.Header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
	//	context.Header("Access-Control-Allow-Credentials", "true")
	//})

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:     cnf.Logger,
		GrpcClient: cnf.GrpcClient,
		Cfg:        cnf.Cfg,
	})

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "Api gateway"})
	})
	// User endpoints
	r.POST("/v1/user/register", handlerV1.RegisterUser)
	r.POST("/v1/user/login", handlerV1.LoginUser)
	r.GET("/v1/user/profile", handlerV1.GetActiveUser)
	r.GET("/v1/user", handlerV1.FindUsers)
	r.PUT("/v1/user/profile", handlerV1.UpdateUserProfile)
	// r.POST("/v1/user/verify-email", handlerV1.VerifyUserEmail)

	// url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	// r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
