#!/bin/bash
CURRENT_DIR=$GOPATH/src/bitbucket.org/illus10n/blog_protos
for x in $(find ${CURRENT_DIR}/* -type d); do
  protoc -I ${CURRENT_DIR} -I /usr/local/include --go-grpc_out=. ${CURRENT_DIR} ${x}/*.proto
done