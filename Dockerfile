# workspace (GOPATH) configured at /go
FROM golang:1.15 as builder


#
RUN mkdir -p $GOPATH/src/bitbucket.org/gateway
WORKDIR $GOPATH/src/bitbucket.org/illus10n/blog_api_gateway

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/blog_api_gateway /

ENTRYPOINT ["/blog_api_gateway"]