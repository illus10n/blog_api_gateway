module bitbucket.org/illus10n/blog_api_gateway

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/casbin/casbin/v2 v2.6.3 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo v1.8.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0 // indirect
	github.com/lib/pq v1.5.2
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/minio/minio-go/v6 v6.0.55 // indirect
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14 // indirect
	github.com/swaggo/gin-swagger v1.2.0 // indirect
	github.com/swaggo/swag v1.6.6 // indirect
	github.com/urfave/cli/v2 v2.2.0 // indirect
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/net v0.0.0-20200528225125-3c3fba18258b // indirect
	golang.org/x/tools v0.0.0-20200529172331-a64b76657301 // indirect
	google.golang.org/grpc v1.29.1
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
